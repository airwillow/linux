- [Console Vs Shell](#console-vs-shell)
  - [Presentation des consoles historiques](#presentation-des-consoles-historiques)
  - [la console en mode graphique (Ctrl + Alt + T)](#la-console-en-mode-graphique-ctrl--alt--t)
  - [Le shell (introduction sommaire)](#le-shell-introduction-sommaire)
  - [Les variables d'environnement (introduction sommaire)](#les-variables-denvironnement-introduction-sommaire)
- [l'invite de commande](#linvite-de-commande)
  - [le manuel](#le-manuel)
  - [Commandes simples](#commandes-simples)
  - [Paramètres](#param%C3%A8tres)
    - [Paramètre courts (une lettre) : précédés d'un tiret](#param%C3%A8tre-courts-une-lettre--pr%C3%A9c%C3%A9d%C3%A9s-dun-tiret)
    - [Les paramètres longs : précédés de 2 tirets](#les-param%C3%A8tres-longs--pr%C3%A9c%C3%A9d%C3%A9s-de-2-tirets)
    - [valeurs des paramètres](#valeurs-des-param%C3%A8tres)
  - [Autocompletion de commande](#autocompletion-de-commande)
  - [Historique des commandes](#historique-des-commandes)
  - [Quelques raccourcis clavier](#quelques-raccourcis-clavier)
- [Le système de fichier](#le-syst%C3%A8me-de-fichier)
  - [Quelques commandes](#quelques-commandes)
    - [pwd](#pwd)
    - [which](#which)
    - [ls](#ls)
    - [cd (pas besoin d'explication)](#cd-pas-besoin-dexplication)
    - [du (disk usage)](#du-disk-usage)
- [Manipulation de fichier](#manipulation-de-fichier)
  - [Affichage](#affichage)
    - [cat](#cat)
    - [less (more)](#less-more)
  - [Création de fichiers et dossiers](#cr%C3%A9ation-de-fichiers-et-dossiers)
    - [touch](#touch)
    - [mkdir](#mkdir)
  - [Copie et déplacement](#copie-et-d%C3%A9placement)
    - [cp](#cp)
    - [mv](#mv)
  - [Utilisation du wildcard ou joker](#utilisation-du-wildcard-ou-joker)
  - [Suppresion de fichiers et dossiers](#suppresion-de-fichiers-et-dossiers)
    - [rm](#rm)
  - [Les liens](#les-liens)
    - [Les liens physiques](#les-liens-physiques)
    - [Les liens symboliques](#les-liens-symboliques)
- [Droits et utilisateurs](#droits-et-utilisateurs)
  - [Root](#root)
    - [SUDO -> Exécuter une commande en tant que root (ou autre user)](#sudo---ex%C3%A9cuter-une-commande-en-tant-que-root-ou-autre-user)
    - [Devenir root](#devenir-root)
    - [Sudoers](#sudoers)
      - [Presentation](#presentation)
      - [Ajouter un utilisateur (simplement)](#ajouter-un-utilisateur-simplement)
      - [En cas de corruption](#en-cas-de-corruption)
      - [Références](#r%C3%A9f%C3%A9rences)
  - [Changer d'utilisateur](#changer-dutilisateur)
  - [Qui suis-je](#qui-suis-je)
  - [Gestion des utilisateurs et des groupes](#gestion-des-utilisateurs-et-des-groupes)
    - [Ajouter un utilisateur](#ajouter-un-utilisateur)
    - [Changer de mot de passe](#changer-de-mot-de-passe)
    - [Supprimer un utilisateur](#supprimer-un-utilisateur)
    - [Modifier un compte utilisateur](#modifier-un-compte-utilisateur)
    - [Ajouter/Supprimer un groupe](#ajoutersupprimer-un-groupe)
  - [Quelques informations en plus sur les utilisateurs](#quelques-informations-en-plus-sur-les-utilisateurs)
    - [Structures des fichiers /etc/passwd et /etc/group](#structures-des-fichiers-etcpasswd-et-etcgroup)
      - [Structure de /etc/passwd](#structure-de-etcpasswd)
      - [Structure de /etc/group](#structure-de-etcgroup)
      - [En vrac](#en-vrac)
  - [Gestion des propriétaires d'un fichier](#gestion-des-propri%C3%A9taires-dun-fichier)
  - [Gestion des droits](#gestion-des-droits)
    - [Présentation](#pr%C3%A9sentation)
    - [Modifier les droits](#modifier-les-droits)
      - [chmod absolu ou octal](#chmod-absolu-ou-octal)
      - [chmod relatif](#chmod-relatif)
    - [Les droits spéciaux](#les-droits-sp%C3%A9ciaux)
      - [setuid](#setuid)
      - [setgid](#setgid)
      - [sticky bit](#sticky-bit)
- [Installation et gestion des paquets](#installation-et-gestion-des-paquets)
  - [Les dépendances](#les-d%C3%A9pendances)
  - [Les dépôts](#les-d%C3%A9p%C3%B4ts)
  - [Gestion des paquets avec APT](#gestion-des-paquets-avec-apt)
- [Recherche de fichiers](#recherche-de-fichiers)
  - [Locate](#locate)
  - [Find](#find)
- [Extraire, trier et filtrer les données](#extraire-trier-et-filtrer-les-donn%C3%A9es)
  - [grep : filtrage](#grep--filtrage)
    - [filtrage sur un fichier](#filtrage-sur-un-fichier)
    - [filtrage sur un dossier](#filtrage-sur-un-dossier)
    - [les expressions régulières](#les-expressions-r%C3%A9guli%C3%A8res)
  - [sort : trier des lignes](#sort--trier-des-lignes)
  - [wc : compter](#wc--compter)

# Console Vs Shell

Connexion sur des machines qui n'ont pas d'interface graphique.  
Plus rapide qu'on ne le croit - certaines taches mettent moins de temps que via une IHM  

```bash
ls -l | grep jpg | wc -l > total.txt
```

## Presentation des consoles historiques

Historiquement 6 terminaux/consoles qui fonctionnent en simultanée (Ctrl+Alt+F1-6) (tty1-6)  
Ctrl+alt+f7 -> retour au mode graphique (ne semble pas fonctionner sous ubuntu + VM, ctrl+Alt+F1 = IHM)  
Chacune des consoles est une session différente, il faut donc se relogguer

## la console en mode graphique (Ctrl + Alt + T)

+ utilisation de d'autres applis graphiques en parallèle
+ resolution d'ecran plus grande
+ personnalisation de l'apparence
+ copier-coller

## Le shell (introduction sommaire)
Le shell est le programme qui est exécuté dans la console (ou terminal).  
Le shell peut être reprsésenté comme la coquille(d'ou le nom de shell) qui entoure le noyau Linux et qui sert de passerelle pour exécuter des commandes, il gère l'invite de commande et attend la saisie des commandes.  
C'est aussi le programme qui se souvient des dernières commandes tapées, qui gère l'autocomplétion, qui gère les processus, définit des alias, ...  
Le shell exécute des commandes internes ou externes (généralement dans /usr/bin comme ls par ex)  

Il existe de nombreux shell :
- **sh** ou **bsh** : Bourne SHell, l'ancêtre de tous les shells du nom de son inventeur
- **bash** : Bourne Again SHell, amélioration de sh et shell par défaut sou Linux et Mac
- **ksh** : Korn SHell
- **csh** : C Shell. Un shell utilisant une syntaxe proche du langage C.
- **tcsh** : Tenex C Shell. Amélioration du C Shell.
- **zsh** : Z Shell. Shell assez récent reprenant les meilleures idées de bash, ksh et tcsh.

## Les variables d'environnement (introduction sommaire)
Les variables d'environnement constituent un moyen d'influencer le comportement des logiciels sur votre système. Par exemple, la variable d'environnement « LANG » détermine la langue que les logiciels utilisent pour communiquer avec l'utilisateur.

```bash
env ou printenv # => affiche la liste des variables d'environnements
set             # => idem 
# différence entre les 2 ? => set est une commande interne au shell et voit donc toutes les variables locales au shell alors que env est une commande externe et ne voit que les variables que le shell lui passe

printenv VAR  # => affiche la valeur de var
echor $VAR    # => idem ci-dessus

export EDITOR=nano   # => affecter nano dans la variable EDITOR (raccourci de EDITOR=nano puis export EDITOR)
```
Pour plus de détails :
https://doc.ubuntu-fr.org/variables_d_environnement

# l'invite de commande

```bash
xxx@yyy:~$
```
xxx : username  
@ : chez  
yyy : nom de l'ordinateur  
~ : dossier courant  
$ / # : utilisateur normal versus root

## le manuel
RTFM 'Read The Fucking Manual' -> man ls

## Commandes simples
```
date
ls
```

## Paramètres
```bash
commande paramètres
```

### Paramètre courts (une lettre) : précédés d'un tiret

```bash
ls -a -l -h
ls -alh
```

(attention au majuscules et minuscules)

### Les paramètres longs : précédés de 2 tirets
Certains paramètres ont les 2 notations : 
```bash
ls -a = ls --all
```
C'est l'application qui décide les paramètres qu'elle prend

### valeurs des paramètres

court : 
```bash
commande -parametre 14 (mysql -u root -p)
```

long : 
```bash
commande --parametre=14
```

Pas de règles absolues dans les paramètres, certains paramètres sont différents et dépendent des commandes
```bash
ls nomdundossier : affiche le contenu d'un dossier
```

## Autocompletion de commande

ex : saisir "da" puis tabulation 
+ si plusieurs résultats : ca les affiche 
+ si un seul : ca complete la commande

## Historique des commandes

- les fleches haut et bas
- la commandes "history" (pour rappeler une commande de l'historique, taper !numerocommande)
- Ctrl+R

## Quelques raccourcis clavier

- Ctrl+L ou clear : efface la console
- Ctrl+D ou exit: commande EOF à la console (ferme une session ou la console)
- Shift+PgUp/Down : pareil que la molette de souris
- Ctrl+A : ramene le curseur au debut
- Ctrl+E : ramene le curseur a la fin
- Ctrl+U : supprime tout ce qui se trouve à gauche
- Ctrl+K : supprime tout ce qui se trouve à droite
- Ctrl+W : supprime le premier mot situé à gauche
- Ctrl+Y : colle un element supprimé

# Le système de fichier

Sous linux, tout est "fichier". Il n'y a pas de lecteur a proprement parler comme sous windows
La particularité de linux est de transformer tout materiels ou périphériques en fichier texte éditable et donc modifiable , bien évidement cela inclut les disques durs.  
Le lecteur cd est un fichier pour linux par ex

Sous linux, la racine du systeme est "/"

dossier | description
--- |---
bin | contient les programmes executables / les commandes de base ((sous Unix, presque toutes les commandes sont "externes", et non intégrées au shell comme sous DOS)
boot | fichier nécessaires au démarrage de la machine.
dev | contient tous les "peripheriques" du système pour lequel le noyau a été configuré  (historiquement par exemple /dev/hda - le premier disque dur IDE du systeme)
etc | la plupart des fichiers de configuration
home | répertoires personnels des utilisateurs
lib | contient des bibliotheques partagées (équivalent des dlls)
lost+found | quand un disque crashe pour une raison ou pour une autre, on utilise le programme fsck pour réparer (équivalent de scandisk); c'est là qu'il dépose les fragments de fichiers perdus.
media | lorsqu'un périphérique amovible (comme une carte mémoire SD ou une clé USB) est inséré dans votre ordinateur, Linux vous permet d'y accéder à partir d'un sous-dossier demedia. On parle de montage.
mnt | c'est un peu pareil quemedia, mais pour un usage plus temporaire.
opt | installation des logiciels commerciaux et addons
proc | répertoire factice, dont les fichiers contiennent des infos sur l'état du système et des processus en cours d'exécution. (cat /proc/version ou cat /proc/cpuinfo)
root | dossier personnel de l'utilisateur « root » 
sbin | contient les commandes systeme (verif et repartion de disque, mise en place du reseau, ...)
tmp | dossier temporaire utilisé par les programmes pour stocker des fichiers.
usr | dossier dans lequel vont s'installer la plupart des programmes demandés par l'utilisateur -> on retrouve une certaine arbo<br><ul><li>bin : les executables</li><li>etc : les fichiers de config</li></ul>
var | dossier contenant des donnees "variables" souvent réécrites comme des logs <ul><li>log : journaux systeme</li><li>lib : bases de données, fichiers de config</li><li>spool : spooler</li><li>run : infos sur les serveurs</li></ul>

## Quelques commandes

### pwd
Affiche le dossier courant  (print working directory)
### which 
Localiser une commande  
```bash
which commande
```

### ls
Liste  le répertoire  
Affichage des couleurs du resultat : ls --color=auto (ou none pour les enlever) (.bashrc)

```bash
ls
ls dossier # => affiche le contenu d'un dossier
ls -a # => tous les fichiers cachés  
ls -F # => type de fichier (si on a pas la couleur par exemple)  
ls -l # => liste détaillée  
    - droits du fichier
    - nombre de liens physiques
    - proprietaire
    - groupe
    - taille du fichier en octets
    - date de derniere modification
    - nom du fichier
ls -h # => human readable (taille de fichier)  
ls -t # => trier par date de derniere modification  
ls -r # => renverse l'odre d'affichage
ls *
```  

```bash
ls -larth
```

Le remplacement par gobbling.  
Il est possible de passer des caractères spéciaux et même des expressions régulières à ls 
```bash
ls *        # => affiche tous les fichiers ainsi que le contenu de tous les dossiers de niveau 1
ls M*       # => affiche les fichiers et le contenu des dossiers commencant par M
ls ???      # => affiche les fichiers et le contenu des dossiers ayant 3 lettres
ls B?????   # => affiche les fichiers et le contenu des dossiers ayant 6 lettres et commencant par B
ls [MP]*    # => affiche les fichiers et le contenu des dossiers commencant par M ou P
```

### cd (pas besoin d'explication) 
cd = cd ~ = retour dans le home directory  
penser à l'autocomplétion avec "tab"

### du (disk usage) 
taille occupée par les sous-dossiers 
```bash
du 
du -h # => human readable (taille des fichiers exprimés en ko ou Mo)  
du -a # => taille des dossiers et des fichiers  
du -s # => avoir juste le total
```

# Manipulation de fichier

Aller dans /var/log et afficher syslog par exemple (log système)

## Affichage

### cat
afficher tout le fichier

```bash
cat fic1
cat -n # => afficher le nombre de lignes
```

### less (more)
afficher le fichier page par page  
la commande more est moins intéressante que less, moins de fonctionnalités, moins rapide et plus vielle
 + espace : affiche la "page" suivante
 + entrée (ou fleche vers le bas) : affiche la ligne suivante
 + d : affiche les 11 lignes suivantes
 + u : affiche les 11 lignes précédentes
 + b : retourne à la "page" précédente
 + y (ou fleche du haut) : affiche la ligne précédente
 + q : arrête la lecture du fichier
 + = : indique l'emplacement dans le fichier (numéro de ligne + pourcentage)
 + h : afficher l'aide
 + / : pour faire une recherche (/ puis le texte puis entrée). Les expressions régulières fonctionnent
 + n : occurence suivante lors d'une recherche
 + N : occurence précédente

 ### tail et head
 Afficher le début et la fin d'un fichier (par défaut 10 lignes)

 ```bash
 tail fic1
 head fic1
 tail -n X # => ou X est un nombre, permet de modifier le nombre de lignes retournées  
 tail -X # => idem tail -n X
 tail -f # => (follow) permet de suivre l'évolution du fichier sur la commande tail (Ctrl + C pour quitter). Le suivi se fait toutes les secondes. 
 tail -f -s X # => modifier le nombre de secondes de suivi
 ```

## Création de fichiers et dossiers

### touch

changer la date de modification d'un fichier ou créer un nouveau fichier.  

```bash
touch fichier1 fichier2 ....
```

### mkdir

création d'un dossier

```bash
mkdir dossier1 dossier2 ....
mkdir -p dossier1/dossier2/dossier3 # => -p pour créer une arborescence complete
```

## Copie et déplacement

### cp
Copier d'un ou plusieurs fichiers/dossiers  (penser à l'autocomplétion)

Copie dans le meme répertoire (2 paramètres : le nom du fichier copié et le nom du fichier résultant)
```bash
cp fichier fichiercopie
```

Copie dans un autre répertoire (2 paramètres : le nom du fichier et le nom du répertoire)
```bash
cp fichier dossier
```

Copie dans un autre répertoire + changement de nom (2 paramètres : le nom du fichier et le nom du répertoire et de fichier)
```bash
cp fichier dossier/fichier2 # => copie de fichier dans dossier avec renommage en "fichier2"
```

```bash
cp -R dossier1 dossier2 # => copie d'un dossier + tous les sous-dossiers + fichiers
```

### mv
Déplacer/renommer un fichier ou un dossier.  
S'utilise de la même facçon que cp (pas besoin d'utiliser -R pour les dossiers)

```bash
mv fichier dossier/
mv dossier1 dossier2
```

mv permet aussi de renommer un fichier
```bash
mv fichier1 fichier2
mv fichier1 dossier/fichier2
```

## Utilisation du wildcard ou joker
Pour plusieurs commandes (dont cp et mv), il est possible d'utiliser le wildcard pour faire une sélection de plusieurs fichiers

```bash
cp *.jpg dossier
cp fic* dossier
```

## Suppresion de fichiers et dossiers

### rm

Pour supprimer un fichier on utilise la commande rm suivi du ou des noms de fichier à supprimer

```bash
rm fichier1 fichier2
```

```bash
rm -i # => demande confirmation  
rm -f # => force la suppression même si problème potentiel  
rm -v # => (verbose) affiche les fichiers en cours de suppression  
rm -r # => suppresion d'un dossier et tout ce qu'il contient  
#(il existe la commande rmdir mais cette commande ne supprime un dossier que s'il est vide)  
```
![alt warning](./warning2.png) Attention à ne pas faire la commande suivante (en fait elle ne marchera en tant que simple user ;)  :
```bash
rm -rf /*
```

## Les liens

Organisation des fichiers dans le systeme de fichiers, très vulgairement :
un fichier est séparé en 3 parties :
- son nom
- les informations de gestion
- le contenu  

Chaque contenu se voit attribué un numéro d'identification : inode

### Les liens physiques
Un lien physique permet d'avoir 2 noms de fichiers qui partagent le même contenu donc le même inode, les 2 fichiers pointent sur le même contenu.

```bash
cd 
mkdir test
cd test
touch fic1
ln fic1 fic2
ls -l
```

La commande ls -l affiche 2 fichier, la seconde colonne affiche 2, cela correspond au nombre de fichier partageant le même inode.

```bash
ls -i # => affiche l'inode de chacun des fichiers
```

Suprrimer un fichier ne supprime pas l'autre et le contenu sera toujours présent. L'inode est supprimé quand plus aucun nom de fichier ne pointe dessus.

### Les liens symboliques
Un lien symbolique est différent. 2 "fichiers" ne poitent plus sur le même inode mais un fichier pointe sur un autre fichier. On créé un lien vers un nom de fichier et non plus vers un inode.

```bash
ln -s fic1 fic2
ls -l
```

la commande ls -l affiche les informations suivantes sur la ligne du fic2 :
- la première lettre est un l (pour link)
- la fin indique une fleche vers le nom de fichier pointé

Les liens symboliques fonctionnent sur les répertoires  
Si on supprime le fichier de référence (ici fic1), alors le lien est mort, fic2 ne pointera plus sur rien.

# Droits et utilisateurs

## Root
root : superutilisateur de la machine->tous les droits contrairement aux autres utilisateurs qui ont des droits limités. Evoluer dans un environnement limité est avant tout une question de sécurité :
- limiter les mauvaises manipulations (erreurs humaines) 
- limiter les failles de sécurité

### SUDO -> Exécuter une commande en tant que root (ou autre user)
Certaines commandes ne sont exécutables qu'en tant que root, il est possible de devenir root un instant avec la commande __SUDO (Substitute User DO / Super User DO)__  (même si la commande sudo est princiapelement utilisée pour exécuter des commandes en tant que superutilisateur, elle peut aussi être utilisée pour exécuter des commandes en tant qu'un autre user en fonction de comment elle est configurée)  
*Attention : Seuls certains utilisateurs appelés __sudoers__ ont le droit d'exécuter la commande sudo.*

__Pourquoi sudo ?__  
Utiliser sudo est meilleur (plus sûr) que d'ouvrir une session en tant que superutilisateur pour un certain nombre de raisons dont :

- Personne n'a à connaitre le mot de passe du superutilisateur (sudo demande le mot de passe de l'utilisateur courant). Des droits supplémentaires peuvent être accordés temporairement à des utilisateurs puis retirés sans qu'il soit besoin de changer de mot de passe.
- Il est facile de n'exécuter que les commandes qui nécessitent des droits spéciaux avec sudo et le reste du temps, on travaille en tant qu'utilisateur non-privilégié, ce qui réduit les dommages que l'on peut commettre par erreur.
- Contrôler et enregistrer : quand une commande sudo est exécutée, le nom de l'utilisateur et la commande sont enregistrés.
  
Pour toutes les raisons ci-dessus, la possibilité de basculer en superutilisateur en utilisant sudo -i (ou sudo su) est habituellement désapprouvé parce que cela annule les avantages cités ci-dessus.

```bash
sudo commande
```
Un mot de passe est demandé : celui de l'utilisateur en cours (prouver que vous êtes l'utilisateur qui fait la demande)  . Il ne sera ensuite plus demandé pendant un certains laps de temps (15 min par defaut)

```bash
sudo -k # => reset le cache de mots de passe de la commande sudo
sudo -l # => pour visualiser à quoi on est autorisé
```

Astuce
```bash
sudo !! # => exécuter la commande précédente en sudo
```

### Devenir root

Pour devenir root, plusieurs commandes disponibles :

```bash
su
su root
su - # => pour adopter l'environnement de root
su - root
```

Sous ubuntu, l'utilisateur root n'est pas actif, il n'est donc pas possible d'exécuter la commande *su root*  

Pour passer root il faut utiliser l'une des commandes suivantes :
```bash
sudo su
sudo -i
```

Il est aussi possible d'activer le compte root (à titre informatif, ce n'est pas indispensable), pour cela il faut lui attribuer un mot de passe :

```bash
sudo passwd
sudo passwd root
```

### Sudoers
#### Presentation
- Le fichier /etc/sudoers décrit les règles de configuration de l'utilisation de la commande sudo.  
- Ce fichier est en lecture seule (même pour root) (l'explication donnée habituellement est que seuls les administrateurs peuvent modifier ce fichier et ce seulement via l'utilitaire __visudo__, il est pourtant possible de le modifier avec un éditeur)  
- Même s'il est possible de modifier directement le fichier, cela est déconseillé, il faut passer par __visudo__
- Pour modifier le fonctionnement de la commande sudo, l'administrateur du système ne modifie plus le fichier /etc/sudoers mais positionne des fichiers de personnalisation dans le répertoire /etc/sudoers.d. Cela a différents avantages :
  - définir autant de fichiers que de modifications
  - pas de droits d'administration pour créer ces fichiers (l'administrateur se contente de le copier dans le répertoire et d'appliquer les droits)
  - apercu des modifications en listant le répertoire
  - annulation d'une autorisation particulière a tout moment en supprimant le fichier de personnalisation correspondant
  - on n'altere pas la configuration initiale de sudo

Choisir l'éditeur par défaut par visudo :
```bash
sudo EDITOR=/usr/bin/nano visudo
```

#### Ajouter un utilisateur (simplement)  
Pour qu'un utilisateur puisse utiliser la commande sudo, comme décrit dans le fichier sudoers, il faut qu'il appartienne au groupe "sudo"
```bash
sudo adduser user sudo
```

#### En cas de corruption
En cas de corruption du fichier sudoers, il n'est plus possible d'utiliser la commande sudo et donc d'éditer le fichier. Tenter les 2 commandes suivantes :

```bash
su -c visudo
pkexec visudo
```

Si cela ne fonctione pas, se rendre sur les liens ci-dessous dans les reférences

#### Références
https://doc.ubuntu-fr.org/sudoers  
https://wiki.debian.org/fr/sudo

## Changer d'utilisateur

Pour agir en tant qu'utilisateur __*user*__ :

```bash
su user
su - user # => - pour adopter l environnement de user
```

## Qui suis-je

Si le prompt n'est pas configuré par défaut, il se peut qu'il n'affiche pas l'utilisateur en cours, il peut donc être interessant de savoir qui on est :

```bash
whoami
```

## Gestion des utilisateurs et des groupes

### Ajouter un utilisateur

```bash
adduser nomDeLUtisateur # => sous debian et ubuntu
useradd nomDeLUtisateur # => commande unix plus généraliste

adduser [-c commentaires] [-d rep_personnel] [-e date_expiration] [-f tps_inactivité]
[-g groupe_initial] [-G groupe[,...] [-m [-k squelette_rep | -M] [-p motdepasse]
[-s shell] [-u uid [-o]] [-n] [-r] utilisateur
```
adduser
- Cette commande crée l'utilisateur ainsi que son répertoire personnel /home/nomDeLUtilisateur
- Le mot de passe pour l'utilisateur est ensuite demandé
- des informations relatives à l'utilisateur sont ensuite demandées
  
 useradd => ne crée pas de mot de passe, le compte n'est donc pas activé 

 La création d'un utilisateur crée aussi un groupe du même nom.
### Changer de mot de passe

```bash
passwd      # => changer le mot de passe de l'utilisateur actuel
passwd user # => changer le mot de passe du l'utilisateur spécifié
```

La  commande passwd a également d'autres fonctions

```bash
passwd --stdin # => la commande abandonne son caractère interactif habituel et examine son entrée standard pour s'en servir comme mot de passe. Très utile dans un script : echo mot | passwd --stdin
passwd -d # => supprimer le mot de passe, l'utilisateur pourra se connecter sans
passwd -l # => pour verrouiller le compte et empecher sa connexion
passwd -u # => pour déverrouiller
```

### Supprimer un utilisateur

```bash
deluser nomDeLUtisateur # => sous debian et ubuntu
userdel nomDeLUtisateur # => commande unix plus généraliste
```

la commande deluser ne supprime le répertoire personnel, pour supprimer le home il faut faire :
```bash
deluser --remove-home nomDeLUtilisateur
userdel -r nomDeLUtilisateur
``` 

### Modifier un compte utilisateur

```bash
usermod [options] utilisateur
```

### Ajouter/Supprimer un groupe
```bash
addgroup nomDuGroupe # => Debian et Ubuntu
groupadd nomDuGroupe # => commande Unix 

delgroup nomDuGroupe # => Debian et Ubuntu
groupdel nomDuGroupe # => commande Unix
```

## Quelques informations en plus sur les utilisateurs

### Structures des fichiers /etc/passwd et /etc/group

Tout ce qui concerne la gestion et l'authentification des utilisateurs est inscrit dans un seul fichier /etc/passwd.  
La gestion des groupes est assurée par /etc/group

Les mots de passe cryptés sont souvent placés dans /etc/shadow, par sécurité lisible seulement par root.


#### Structure de /etc/passwd

Ce fichier comprend 7 champs, séparés par le symbole « : »

1. nom de connexion (encore appelé nom d'utilisateur ou login)
2. ancienne place du mot de passe crypté
3. numéro d'utilisateur uid, sa valeur est le véritable identifiant pour le système Linux; l'uid
de root est 0, le système attribue conventionnellement un uid à partir de 500 aux comptes créés.
4. numéro de groupe gid, dans lequel se touve l'utilisateur par défaut; le gid de root est 0, les groupes d'utilisateurs au delà de 500
5. nom complet, il peut être suivi d'une liste de renseignements personnels (cf chfn)
6. rép. personnel (c'est également le rép. de connexion)
7. shell, interprétateur de commandes (par défaut /bin/bash)

#### Structure de /etc/group

Ce fichier comprend 4 champs, séparés par le symbole « : »

1. nom du groupe
2. x pour remplacer un mot de passe non attribué maintenant
3. numéro de groupe, c-à-d l'identifiant gid
4. la liste des membres du groupe

#### En vrac

Pour Connaitre l'uid et le gid de l'utilisateur courant, on utilise la commande id dont le résultat ressemble à ce qui suit :

```bash
id
# => uid=501(stage1) gid=501(stage1) groups=501(stage1), 504(stagiaire)
```

Pour décrire un utilisateur on utilise la commande
```bash
chfn
```

Pour afficher tout simplement la liste des utilisateurs
```bash
cat /etc/passwd | awk -F: '{print $ 1}'
```


## Gestion des propriétaires d'un fichier

```bash
chown user fichier      # => "change owner" (change le propriétaire) : user devient le propriétaire de fichier
chgrp group fichier     # => "change group" (affecte le groupe) : group devient le groupe affecté à fichier
chown user:group fichier # => changement de propriétaire et de groupe du fichier
chown -R user:group dossier # => applique le changement récursivement à tous les sous-dossiers et fichiers
```
## Gestion des droits

### Présentation 
Chaque fichier/dossier possède une liste de droits visibles avec la commande **ls -l** : il s'agit de la première colonne de gauche dans le résultat :

![alt droits](./droits.png)

La première colone indique le type de fichier :
- d (directory) : c'est un dossier
- l (link) : c'est un dossier
- \- (vide) : c'est un fichier
- s (socket)
- p (pip) : pipeline ou tube
- ...

Les colonnes suivants sont composées des lettres suivantes :
- r (Read) : droit en lecture
- w (Write) : droit en écriture
- x (eXecute) : droit d'exécution pour un fichier / droit de traverser(afficher le contenu) un dossier

Si la lettre apparait alors le droit existe sinon un - est affiché

Les droits sont découpés en fonction des utilisateurs :

![alt droits découpage](./droitdecoupage2.png)

**root a tous les droits**

### Modifier les droits

2 méthodes pour changer les droits : chmod absolu et chmod relatif

#### chmod absolu ou octal

Cette méthode utilise un nombre à 3 chiffres XYZ (numérotation octale = en base 8), chaque chiffre déterminant :
- X : les droits utilisateurs
- Y : les droits groupe
- Z : les droits "others"

Chaque chiffre est la somme de la pondération de chacun des droits :
- read : 4
- write : 2
- execute : 1

Liste des droits possibles :  


| Droits | Chiffre | Calcul |
| --- | --- |--- |
|\---|0|0+0+0|
|r--|4|4+0+0|
|-w-|2|0+2+0|
|--x|1|0+0+1|
|rw-|6|4+2+0|
|-wx|3|0+2+1|
|r-x|5|4+0+1|
|rwx|7|4+2+1|

Ainsi en en combinant les 2, nous obtenons un nombre à 3 chiffres décrivant précisémment les droits d'un fichier.  
Par exemple, 640 
- 6 : droit de lecture et d'écriture pour le propriétaire
- 4 : droit de lecture pour le groupe
- 0 : aucun droit pour les autres

777 -> tous les droits
000 -> aucun droit (sauf root)

```bash
chmod 640 fichier # => attribue les droits 640 au fichier
chmod -R 640 fichier # => récursif a tous les fichiers et dossiers
```
#### chmod relatif
Cette méthode utilise des lettres pour attribuer les droits  
Paramétrage droit par droit

- u : user
- g : group
- o : other

- \+ : ajouter le droit
- \- : retirer le droit
- = : affecter le droit

```bash
chmod g+w fichier # => ajoute le droit en écriture à "group"
chmod o-r fichier # => enlève le droit de lecture à "other"
chmod o+rx fichier # => ajoute les droirs en lecture et écriture à "other"
chmod g+w,o-w fichier # => ajoute écriture a groupe, enleve ecriture à other
chmod +x fichier # => ajoute exécution à tout le monde
chmod u=rwx,g=r,o=- # => tous les droits a user, lecture a group, rien à other
```
### Les droits spéciaux
Les droits sont parfois spécifiés avec 4 chiffres : 0777  
Il existe 3 droits spéciaux : *setuid*, *setgid*, *stickybit*

Par exemple aller voir dans 
```bash
ls -l /usr/bin
```

#### setuid
Ce droit s'applique aux fichiers exécutables, il permet d'allouer temporairement à un utilisateur les droits du propriétaire du fichier, durant son exécution.  
*L’exemple le plus courant est l’utilisation de la commande passwd. Lorsqu’on souhaite changer son mot de passe, c’est cette commande qu’on utilise. Mais pour que le changement de mot de passe soit effectif, il faut que passwd puisse écrire dans les fichiers. /etc/passwd et /etc/shadow. Problème, en tant qu’utilisateur nous n’avons pas le droit d’écrire dans ces fichiers. C’est là que SETUID entre en action. On dit alors que passwd est « setuidé » Celà veut dire que passwd est lancé avec les droits de root et du coup en tant qu’utilisateur l’écriture dans les fichiers précédemment cités est donc possible.*

Ce droit a la valeur octale 4000

```bash
-rwsr-xr-x  1 root   root     155008 févr. 10  2014 sudo* # il y a un "s" à la place du x
```

#### setgid
Ce droit fonctionne comme le droit SUID, mais appliqué aux groupes. Il donne à un utilisateur les droits du groupe auquel appartient le propriétaire de l'exécutable et non plus les droits du propriétaire.

Ce droit a la valeur octale 2000

```bash
-rwxr-sr-x  1 root   ssh      284784 mai   12  2014 ssh-agent* # il y a un "s" a la place du x
```
#### sticky bit
Lorsque ce droit est positionné sur un répertoire, il interdit la suppression d'un fichier qu'il contient à tout utilisateur autre que le propriétaire du fichier.

Ce droit a la valeur octale 1000

```bash
drwxrwxrwt   86 root root  20480 tmp # il y a un "t" à la fin
```

# Installation et gestion des paquets

Ce qui va suivre ne concerne que les distributions basées sous debian.

- Sous ubuntu => pas de programmes d'installations (comme sous windows) mais des **paquets**
- Un paquet est une sorte de dossier zippé qui contient tous les fichiers du programme. 
- extension **.deb** en reférence à DEBian. 
- Il contient toutes les instructions nécessaires pour installer le programme.
- Contient une gestion de **dépendances** du programme.
- Généralement, ces paquets sont disponibles sur un serveur : **dépôt** (repository)

## Les dépendances

Un programme fonctionne rarement seul, il a besoin de d'autres programmes ou de bibliothèques pour fonctionner => ce sont ses dépendances

Chaque paquet indique de quel autre paquet il dépend. Ainsi lorsqu'on installe un paquet, le système cherche automatiquement les paquets nécessaires.

## Les dépôts

Serveurs sur lesquels sont centralisés des paquets.  
En général, avoir un seul bon dépôt suffit à télécharger la plupart des programmes sous linux.

![alt droits dépots](./depot.png)

La liste des dépôts se trouve dans le fichier /etc/apt/sources.list

Le répertoire /etc/apt/sources.list.d/ contient des configurations vers d'autres serveurs

```bash
# Structure du fichier sources.list
deb url-du-miroir branche section1 (section 2 section 3) # => paquets binaires
deb-src url-du-miroir branche section1 (section 2 section 3) # => sources

# Une branche peut être désignée sous son nom de code ou son nom d'archive
```

## Gestion des paquets avec APT
La gestion des paquets se fait par l'outil APT (advanced Packaging Tool).

Il existe d'autres utilitaires comme aptitude et apt-get (encore très largement utilisé et dans certains cas plus complet) mais nous utiliserons apt.  
Voir ici pour plus d'infos sur les autres outils : https://debian-facile.org/doc:systeme:apt:clients

```bash
# actualiser la liste des paquets disponibles du système
apt update

# installer la derniere version des paquets (doit toujours être précédé d'un update)
apt upgrade

# installer un nouveau paquet provenant d'un dépot du sources.list (il faut renseigner le nom exact du paquet)
apt install <paquet>
apt install <paquet1> <paquet2> <paquet3>

# installer un paquer téléchargé depuis un site
apt install paquet.deb
dpkg -i paquet.deb # autre méthode avec un autre utilitaire

# supprimer un paquet (supprime aussi les dépendances non nécessaires)
apt remove <paquet>
apt purge <paquet> # supprime aussi les fichiers de configuration

# editer le fichier sources.list (coloration syntaxique + vérification de bases)
apt edit-sources

# rechercher un paquet
apt search <terme>
apt-cache search <terme> # variante

# mise a jour pour l'ensemble du système
apt full-upgrade # identique a upgrade mais peut aussi supprimer des paquets pour résoudre des conflits

# autres commandes
apt list                # => liste des paquets installés
apt list --upgradable   # => liste des paquets qui seront mis à jour
apt show <paquet>       # => informations sur un paquet
apt list --all-versions <paquet> # => afficher toutes les versions disponibles pour un paquet
```

A noter qu'il existe un utilitaire **apt-key** qui gère les clés dont se sert APT pour authentifier les paquets (les paquets authentifiés sont réputés fiable).  
Généralement l'ajout d'un repository s'accompagne de l'ajout d'une clé via la commande :

```bash
apt-key add fichierCle 
```

aptkey prend en charge le format binaire OpenGPG (extension .gpg).

# Recherche de fichiers

## Locate

```bash
locate nomDeFichier
```

Locate est une recherche très rapide.  

Inconvénients :
- recherche dans une base de données de fichiers et non sur le disque (les nouveaux fichiers créés ne seront pas immédiatement visible)
- recherche dans toute le disque (pas très précis, impossible de cibler un répertoire)

Il est possible de forcer la reconstruction de la base de données (au lieu d'attendre sa mise à jour automatique)

```bash
updatedb
```

```bash
# tester en créant un nouveau fichier
touch fic1
locate fic1
```
**Exclusion de répertoire de l'indexation**
Pour exclure un répertoire de l'indexation (car inutile, couteux en perf et en espace disque), il faut se rendre dans le fichier : */etc/updatedb.conf* et ajouter le répertoire dans **PRUNEPATHS**

## Find

Contrairement à locate, find recherche directement sur le disque dur, find est donc plus long que locate mais peut être plus pertinent.  
Find permet également de faire d'autres opérations.

```bash
find <ou> <quoi> <action> # seul le paramètre quoi est obligatoire
```

- *ou* : nom du dossier dans lequel rechercher (si non précisé, alors il s'agira du dossier courant)
- *quoi* : le fichier à rechercher (par son nom, sa taille, sa date, ...) / **Obligatoire**
- *action* : post-traitement, action à exécuter sur les fichiers trouvés (si non précisé, les fichiers sont affichés)

```bash
# PAR NOM
find -name "fichier"  # => recherche à partir du nom
# l'utilisation de "" est préférable voir indispensable lorsqu'on utilise des caractères spéciaux comme * ou espace

find /var/log -name "syslog" # => recherche "syslog" exactement dans /var/log
find /var/log -name "syslog*" # => recherche tous les noms de fichier commencant par syslog dans /var/log


# PAR TAILLE
find ~ -size +10M   # => recherche les fichiers de + de 10 Mo
find ~ -size -100k  # => recherche les fichiers de - de 100 K
find ~ -size 1M     # => exactement 1 Mo


# PAR DATE D'ACCES / DE CREATION / DE MODIFICATION
find -name "*.txt" -atime 6 # => les fichiers txt accédés il y a 7 jours
find -name "*.txt" -ctime -1 # => les fichiers txt créés depuis moins d'2 jours
find -name "*.txt" -mtime +5 # => les fichiers txt modifiés il y a plus de 6 jours
# le compteur commence à 0 pour les fichiers créés dans les 24h


# PAR TYPE
find /var/log -name "syslog" -type d # => recherche les dossiers
find /var/log -name "syslog" -type f # => recherche les fichiers


# PAR DROITS
find /sbin -perm 2755


# PAR USER
find /var -user mysql


# PLUS RECENT
find ~ -newer ~/test    # => les fichiers plus récents que le fichier test dans le répertoire home


# LIMITER l'ETENDUE DE LA RECHERCHE
find /home -mindepth 1 -maxdepth 1 -type d
```

Qu'en est-il de l'action ? Par défaut l'action exécutée est **-print** pour afficher les résultats.  

```bash
find -name "*.txt" -print   # => équivalent à find -name "*.txt*
find . -name "*.jpg" -printf "%p - %u\n" # => affiche le résultat formatée (man find)
find . -name "*.jpg" -ls # => affiche le résultat avec détail
find -name "*.jpg" -delete  # => supprime les jpg trouvés dans le repertoire courant
```

Appler une commande avec **-exec** (ou -**-ok** pour avoir demande de confirmation)

```bash 
find -name "*.jpg" -exec chmod 600 {} \; # => appliquer les droits 600 à tous les jpg
find /etc/ -type d -name RCS -exec tar uvf ~/archives_rcs.tar {} \; # => archiver tous les répertoires RCS de votre répertoire « /etc »
```

- cette commande ne doit PAS être entre guillemets
- les accolades {} seront remplacées par le nom du fichier
- la commande doit finir par un \; obligatoirement

# Extraire, trier et filtrer les données

## grep : filtrage

Rôle principal : recherche d'un mot dans un fichier  
-> retourne les lignes contenant le mot en question

### filtrage sur un fichier

```bash
grep texte nomFichier # => recherche texte dans nomFichier

# exemple
grep alias .bashrc    # il n'est pas nécessaire de mettre des "" autour du texte a chercher sauf s'il contient des espaces

# quelques options 
grep -i alias .bashrc # => ne pas tenir compte de la casse
grep -n alias .bashrc # => afficher les numéros de lignes
grep -v alias .bashrc # => inverser le filtrage : ignorer un mot
grep -l alias *       # => list file only, affiche juste les noms des fichiers contenant la chaine
grep -3 alias .bashrc # => affiche les 3 (n) lignes précédentes et suivantes
```

### filtrage sur un dossier

```bash
grep -r texte nomDossier
```

### les expressions régulières

Il existe 2 types d'expressions régulières :

- Les expressions régulières basiques (ERb)
- Les expressions régulières étendues (ERe)  

Les ERb sont utilisées par les commandes suivantes :

- vi
- grep
- expr
- sed

Les ERe sont utilisées par les commandes suivantes :

- grep avec l'option -E (egrep)
- awk

Caractères communs aux ERb et ERe

| Caractère spécial	| Signification |
|---|---|
|^|	Début de ligne
|$|	Fin  de ligne
|. (point)|	Un caractère quelconque
|[liste_de_caractères]|	Un caractère cité dans la liste
|[^liste_de_caractères]|	Un caractère qui n'est pas cité dans la liste
|*|	0 à n fois le caractère ou regroupement précédent
|\<expression|	Début d'un mot. Caractères pouvant faire partie d'un mot : [A-Za-z0-9_]
|expression\>|	Fin d'un mot
|\<expression\>|	Mot complet

Caractères spécifiques aux ERb

| Caractère spécial	| Signification |
|---|---|
|\{n\}|	n fois le caractère précédent
|\{n,\}|	Au moins n fois le caractère précédent
|\{n,x\}|	Entre n et x fois le caractère précédent
|\(ERb\)|	Mémorisation d'une ERb
|\1, \2, ...|	Rappel de mémorisation

Caractères spécifiques aux ERe

| Caractère spécial	| Signification |
|---|---|
|?|	0 ou 1 fois le caractère ou regroupement précédent
|+|	1 à n fois le caractère ou regroupement précédent
|{n}|	n fois le caractère ou regroupement précédent
|{n,}|	Au moins n fois le caractère ou regroupement précédent
|{n,x}|	Entre n et x fois le caractère ou regroupement précédent
|(er1)|	Regroupement
|er1 \| er2 \| er3|	Alternatives

fichier de test : 
https://www.insee.fr/fr/statistiques/fichier/2114819/depts2016-txt.zip

Convertir le fichier récupéré au format unix (commande "dos2unix")

Quelques exemples

```bash
grep -E '^75' depts2016.txt                 # => Chaine commencant par 75

grep -E 'yotte$' depts2016.txt              # => Chaine finissant par yotte

grep -E '^[+-]?[0-9]+$' depts2016.txt       # => Chaine représentant un nombre entier d'au moins 1 chiffre précédé éventuellement du signe + ou -

gep -E '\-[a-zA-Z]{2,5}\-' depts2016.txt    # => Chaine contenant entre 2 et 5 caractères majuscule ou minuscule entourés de -

grep -E '^75|yotte$' depts2016.txt          # => Chaine commencant par 75 ou finissant par yotte

grep -E '(in|re)+' depts2016.txt            # => Chaine composée de 1 à n occurences de "in" ou de "re" (en minuscule)
```

## sort : trier des lignes

Permet de trier les lignes d'un fichier

```bash
sort depts2016.txt

sort -o depts2016.txt dpttries.txt      # => écrit le resultat dans un fichier
sort -o depts2016.txt dpttries.txt      # => écrit le resultat dans un fichier
sort -o depts2016.txt dpttries.txt      # => écrit le resultat dans un fichier

sort -r depts2016.txt                   # => trier en ordre inverse
sort -R depts2016.txt                   # => trier aléatoirement

sort -n depts2016.txt                   # => trier des nombres
```

## wc : compter